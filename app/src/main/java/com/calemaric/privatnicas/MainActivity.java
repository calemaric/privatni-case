package com.calemaric.privatnicas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private EditText gornjiTekst = null;
    private EditText donjiTekst = null;
    private Button dugme = null;

    private TextView tekst = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gornjiTekst = (EditText) findViewById(R.id.gornjiText);
        donjiTekst = (EditText) findViewById(R.id.donjiText);

        dugme = (Button) findViewById(R.id.button);

        tekst = (TextView) findViewById(R.id.tekst);
    }

    public void onButtonClick(View view) {
        String gornji = gornjiTekst.getText().toString();
        String donji = donjiTekst.getText().toString();

        if(!gornji.isEmpty() && !donji.isEmpty()) {
            tekst.setText(gornji + " " + donji);
        } else {
            tekst.setText("");
        }
    }

    public void onPrelazClicked(View view) {
        Intent i = new Intent(this,TableActivity.class);
        startActivity(i);
    }
}
